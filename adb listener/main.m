//
//  main.cpp
//  adb listener
//
//  Created by Tony Hoyle on 14/04/2013.
//  Copyright (c) 2013 Tony Hoyle.  Covered by the 'Do what the hell you want with this code' license
//

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

NSString *fakeBundleIdentifier = nil;

@implementation NSBundle(swizle)

// Overriding bundleIdentifier works, but overriding NSUserNotificationAlertStyle does not work.

- (NSString *)__bundleIdentifier
{
    if (self == [NSBundle mainBundle]) {
        return fakeBundleIdentifier ? fakeBundleIdentifier : @"com.apple.finder";
    } else {
        return [self __bundleIdentifier];
    }
}

@end

BOOL installNSBundleHook()
{
    Class class = objc_getClass("NSBundle");
    if (class) {
        method_exchangeImplementations(class_getInstanceMethod(class, @selector(bundleIdentifier)),
                                       class_getInstanceMethod(class, @selector(__bundleIdentifier)));
        return YES;
    }
	return NO;
}

int main(int argc, const char * argv[])
{
    struct addrinfo ai = {0};
    struct addrinfo *res;
    struct sockaddr_storage sa;
    socklen_t salen;
    char message[256];
    char address[INET6_ADDRSTRLEN];
    NSString *lastId;
    
    NSUserDefaults *args = [NSUserDefaults standardUserDefaults];
    NSString *adbPath = [args stringForKey:@"adbpath"];
    if(!adbPath)
    {
        NSLog(@"Missing -adbpath argument");
        return 1;
    }
    
    installNSBundleHook();
    
    int s = socket(AF_INET, SOCK_DGRAM, 0);
    
    //Resolve host address
    ai.ai_family = AF_INET;
    ai.ai_protocol = IPPROTO_UDP;
    ai.ai_socktype = SOCK_DGRAM;
    ai.ai_flags = AI_PASSIVE;
    
    if(getaddrinfo(NULL, "31840", &ai, &res))
    {
        NSLog(@"Unable to getaddrinfo\n");
        return 1;
    }

    if(bind(s, res->ai_addr, res->ai_addrlen))
    {
        NSLog(@"Unable to bind\n");
        return 1;
    }
    
    freeaddrinfo(res);
    
    while(1)
    {
        salen = sizeof(sa);
        ssize_t len = recvfrom(s, message, sizeof(message), 0, (struct sockaddr *)&sa, &salen);
        if(len < 0)
        {
            NSLog(@"recvfrom failed\n");;
            continue;
        }
        if(!memcmp(message, "ADBMNGR:", 8))
        {
            NSString *msg = [NSString stringWithUTF8String:message+8];
            NSArray *data = [msg componentsSeparatedByString:@"|"];
            NSString *sport, *name, *id;
            
            sport = [data objectAtIndex:0];
            id = [data objectAtIndex:1];
            name = [data objectAtIndex:2];
            
            if ([id compare:lastId] != NSOrderedSame)
            {
                if(getnameinfo((struct sockaddr*)&sa,salen,address,sizeof(address),0,0,NI_NUMERICHOST)<0)
                {
                    NSLog(@"failed to convert address to string\n");
                    return 1;
                }
                
                NSUserNotification *notification = [[NSUserNotification alloc] init];
                [notification setTitle:[NSString stringWithFormat:@"%@ ADB available", name]];
                [notification setInformativeText:[NSString stringWithFormat:@"%s:%@", address, sport]];
                [notification setDeliveryDate:[NSDate date]];
                [notification setSoundName:NSUserNotificationDefaultSoundName];
                NSUserNotificationCenter *center = [NSUserNotificationCenter defaultUserNotificationCenter];
                [center scheduleNotification:notification];
                
                NSTask *task = [[NSTask alloc] init];
                NSArray *arguments = [NSArray arrayWithObjects:@"connect",
                                      [NSString stringWithFormat:@"%s:%@", address, sport], nil];
                [task setLaunchPath:adbPath];
                [task setArguments:arguments];
                [task launch];
                
                lastId = id;
            }
        }
    }
    
    return 0;
}

